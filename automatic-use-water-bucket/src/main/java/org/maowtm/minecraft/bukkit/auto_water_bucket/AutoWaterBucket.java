package org.maowtm.minecraft.bukkit.auto_water_bucket;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.plugin.java.JavaPlugin;

final class GeneratedBlockPlaceEvent extends BlockPlaceEvent {
	Runnable revert_inv_change;
	GeneratedBlockPlaceEvent(Block placedBlock, BlockState replacedBlockState, Block placedAgainst, ItemStack itemInHand, Player thePlayer, boolean canBuild, EquipmentSlot hand, Runnable revert_inv_change) {
		super(placedBlock, replacedBlockState, placedAgainst, itemInHand, thePlayer, canBuild, hand);
		this.revert_inv_change = revert_inv_change;
	}
}

public final class AutoWaterBucket extends JavaPlugin implements Listener {
	@Override
	public void onEnable() {
		getServer().getPluginManager().registerEvents(this, this);
	}

	@Override
	public void onDisable() {}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void onDamage(EntityDamageEvent evt) {
		if (evt.getEntityType().equals(EntityType.PLAYER) && evt.getCause().equals(DamageCause.FALL)) {
			Player player = (Player) evt.getEntity();
			if (shouldApplyToPlayer(player)) {
				final PlayerInventory inv = player.getInventory();
				ItemStack current_held = inv.getItemInMainHand();
				if (current_held.getType().equals(Material.WATER_BUCKET)) {
					final ItemStack current_held_ = current_held;
					doPlaceWater(player, inv, current_held, () -> {
						inv.setItemInMainHand(new ItemStack(Material.BUCKET));
						evt.setCancelled(true);
					}, () -> {
						inv.setItemInMainHand(current_held_);
					});
					return;
				}
				current_held = inv.getItemInOffHand();
				if (current_held.getType().equals(Material.WATER_BUCKET)) {
					final ItemStack current_held_ = current_held;
					doPlaceWater(player, inv, current_held, () -> {
						inv.setItemInOffHand(new ItemStack(Material.BUCKET));
						evt.setCancelled(true);
					}, () -> {
						inv.setItemInOffHand(current_held_);
					});
					return;
				}
				for (int i = 0; i < 9; i ++) {
					final ItemStack it = inv.getItem(i);
					if (it != null && it.getType().equals(Material.WATER_BUCKET)) {
						final int _i = i;
						doPlaceWater(player, inv, it, () -> {
							inv.setItem(_i, new ItemStack(Material.BUCKET));
							inv.setHeldItemSlot(_i);
							evt.setCancelled(true);
						}, () -> {
							inv.setItem(_i, it);
						});
						return;
					}
				}
			}
		}
	}

	private void doPlaceWater(Player player, PlayerInventory inv, ItemStack water_bucket, Runnable set_to_bucket, Runnable revert_inventory_change) {
		Location loc = player.getLocation();
		Block b = loc.getBlock();
		Block b_against = loc.add(0, -1, 0).getBlock();
		BlockState old_state = b.getState();
		if (b.getType().equals(Material.AIR)) {
			b.setType(Material.WATER);
			set_to_bucket.run();
			getServer().getPluginManager()
				.callEvent(new GeneratedBlockPlaceEvent(b, old_state, b_against, water_bucket, player, true, EquipmentSlot.HAND, revert_inventory_change));
		}
	}

	boolean shouldApplyToPlayer(Player p) {
		// todo
		return true;
	}

	@EventHandler(ignoreCancelled = false, priority = EventPriority.MONITOR)
	public void blockPlaceOutcomeObserver(GeneratedBlockPlaceEvent evt) {
		if (evt.isCancelled()) {
			evt.getBlockReplacedState().update(true);
			evt.revert_inv_change.run();
		}
	}
}
